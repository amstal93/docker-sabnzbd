# docker-sabnzbd

[![pipeline status](https://gitlab.com/cusoon/docker-sabnzbd/badges/main/pipeline.svg)](https://gitlab.com/cusoon/docker-sabnzbd/commits/main) 
[![Sabnzbd Release](https://gitlab.com/cusoon/docker-sabnzbd/-/jobs/artifacts/main/raw/release.svg?job=PublishBadge)](https://gitlab.com/cusoon/docker-sabnzbd/-/jobs/artifacts/main/raw/release.txt?job=PublishBadge)

This is a alpine-based dockerized build of sabnzbd.


## Example usage

docker run example
````
docker run -d \
  -v [/configdir]:/config \
  -v [/completedir]:/complete \
  -v [/incompletedir]:/incomplete \
  -p 8080:8080 \
  --restart=unless-stopped thebungler/sabnzbd
````

docker-compose example:
````
---
version: "2.1"
services:
  sabnzbd:
    image: thebungler/sabnzbd
    container_name: sabnzbd
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/Berlin
    volumes:
      - /path/to/data:/config
      - /path/to/downloads:/downloads #optional
      - /path/to/incomplete/downloads:/incomplete-downloads #optional
    ports:
      - 8080:8080
    restart: unless-stopped
````

## Authors and acknowledgment
More information about sabnzbd can be found here:
[SABnzbd](https://sabnzbd.org "SABnzbd Project Homepage") 

## Project status
I am trying to keep this updated, as i am using the image myself for my homeserver. However, expect nothing.

